const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: [
        path.join(__dirname, 'assets', 'js', 'app.js'),
        path.join(__dirname, 'assets', 'scss', 'app.scss')
    ],
    output: {
        filename: 'bundle.js'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "style.css"
        }),
    ],
    module: {
        rules: [{
            test: /\.scss$/,
            use: [{
                    loader: MiniCssExtractPlugin.loader
                },
                "css-loader",
                "sass-loader"
            ]
        }]
    }
};